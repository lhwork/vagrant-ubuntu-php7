#!/usr/bin/env bash

# add new repositories

#sudo su -

#rm -r /etc/apt/sources.list
sudo mv /etc/apt/sources.list /etc/apt/sources.list.old

sudo echo "deb http://mirrors.aliyun.com/ubuntu/ xenial main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ xenial-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ xenial-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ xenial-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ xenial-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ xenial main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-backports main restricted universe multiverse" >> /etc/apt/sources.list

locale-gen zh_CN.UTF-8

sudo apt-get update > /dev/null

# utility to be able to use apt-add-repository

# php7
sudo apt-get install software-properties-common -y
sudo apt-get install python-software-properties -y
#sudo apt-get install build-essential -y
#export LC_ALL=C.UTF-8

#sudo add-apt-repository ppa:ondrej/php -y
sudo LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y

sudo apt-get update > /dev/null

# install mysql
#apt-get install debconf-utils -y

#debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
#debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

#apt-get install mysql-server -y


# install php7 nginx required packages
sudo apt-get install -y php7.0 php7.0-fpm php7.0-mysql php-mongodb php-redis php7.0-curl php7.0-mcrypt php7.0-gd php7.0-cli php-xdebug php7.0-intl php-memcached php7.0-mbstring php-imagick php7.0-imap php7.0-xml php7.0-bcmath php7.0-json nginx curl

sudo apt-get install -y git vim

sudo apt-get clean all

# install composer
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php
sudo php -r "unlink('composer-setup.php');"

sudo mv /home/vagrant/composer.phar /usr/local/bin/composer
sudo composer config -g repo.packagist composer https://packagist.phpcomposer.com
sudo composer global require "fxp/composer-asset-plugin:^1.2.0"
sudo composer self-update
sudo composer global update --prefer-dist

sudo cp /vagrant/nginx/* /etc/nginx/conf.d/

#service nginx reload
#service php7.0-fpm restart

#apt-get install git -y

# install autojump https://github.com/wting/autojump
#git clone git://github.com/joelthelion/autojump.git /home/vagrant/autojump
#cd /home/vagrant/autojump && ./install.py
